/*
 *
 *  TOTP: Time-Based One-Time Password Algorithm
 *
 *  Original version by fmount <fmount9@autistici.org>
 *  This version uses the mbedTLS library instead of openSSL
 *
 *  This software is distributed under MIT License
 *
 */

#ifndef RFC4226_H
#define RFC4226_H

#include<stdint.h>
#include<stdlib.h>

//MAIN HOTP function
uint32_t HOTP(uint8_t *key, size_t kl, uint64_t interval, int digits);
//First step
int hmac(unsigned char *key, int kl, uint64_t interval, uint8_t* output);
//Second step
uint32_t DT(uint8_t *digest);

#endif
