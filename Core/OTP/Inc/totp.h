/*
 *
 *  TOTP: Time-Based One-Time Password Algorithm
 *
 *  Original version by fmount <fmount9@autistici.org>
 *  This version uses the mbedTLS library instead of openSSL
 *
 *  This software is distributed under MIT License
 *
 */

#ifndef INC_TOTP_H_
#define INC_TOTP_H_

#include <stdint.h>
#include <time.h>

#define TNULL 0
#define DIGITS 6
#define VALIDITY 30

#define ALGO "SHA1"
#define KEYSIZE 20 // bytes

typedef struct user {
  char name[16];
  uint8_t key[KEYSIZE];
} user;

void SetTimeZone(int tz);

int ShareSecretKey(const char* issuer, const char* user, char* uri);
uint32_t GetCode(const char* user, int d, int m, int y, int h, int M, int s);

#endif /* INC_TOTP_H_ */
