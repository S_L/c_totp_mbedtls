/*
 *
 *  TOTP: Time-Based One-Time Password Algorithm
 *
 *  Original version by fmount <fmount9@autistici.org>
 *  This version uses the mbedTLS library instead of openSSL
 *
 *  This software is distributed under MIT License
 *
 */

#include "totp.h"

#include <time.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "base32.h"
#include "rfc6238.h"

user exampleUser = {
	.name = "Admin",
	.key = { 'H', 'e', 'l', 'l', 'o', '!', (uint8_t) 0xDE, (uint8_t) 0xAD, (uint8_t) 0xBE, (uint8_t) 0xEF }
};

int timezone = 1;


void SetTimeZone(int tz)
{
	timezone = tz;
}

int ShareSecretKey(const char* issuer, const char* user, char* uri)
{
	char key32[2*KEYSIZE] = {0};
	base32_encode(exampleUser.key, KEYSIZE, (uint8_t*)key32, sizeof(key32));

	sprintf(uri,"otpauth://totp/%s:%s?secret=%s&issuer=%s&algorithm=%s&digits=%d&period=%d", issuer, user, key32, issuer, ALGO, DIGITS, VALIDITY);

	return 0;
}

uint32_t GetCode(const char* user, int d, int m, int y, int h, int M, int s)
{
	struct tm datetime;
	datetime.tm_hour = h-timezone;
	datetime.tm_min = M;
	datetime.tm_sec = s;
	datetime.tm_mday = d;
	datetime.tm_mon = m-1;
	datetime.tm_year = y >= 1900 ? (y-1900) : y;

	time_t t = floor((mktime(&datetime) - TNULL) / VALIDITY);

	if (strcmp(user, exampleUser.name) == 0) return TOTP(exampleUser.key, KEYSIZE, t, DIGITS);

	return -1;
}
