**Time-Based One-Time Password (TOTP) C-implementation for Microcontrollers using mbedTLS**

The library implements [RFC4226](https://tools.ietf.org/html/rfc4226) and [RFC6238](https://tools.ietf.org/html/rfc6238). 

**The library can be used independently of this example.**

The example is a STM32 CUBE IDE project for the STM32F429I Discovery board, but that can easily be changed.
Keep in mind to update the [mbedTLS](https://github.com/ARMmbed/mbedtls) library.
The usage of this library can be seen in the two files _main.c_ and _totp.c_.

This project is based on the [c_otp](https://github.com/fmount/c_otp) project that uses OpenSSL.
